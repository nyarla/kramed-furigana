"use strict";

var __is_applied = false;

(function () {
    if ( __is_applied) {
        return;
    }

    var _utils  = require('kramed/lib/utils');
    var replace = _utils.replace;
    var merge   = _utils.merge;
    
    var FURIGANA    = /^[{]([^}]+?)[}]/;
    var INSERT_CODE = (function () {/*
// furigana
    if ( cap = this.rules.furigana.exec(src) ) {
        var self = this;

        src = src.substring(cap[0].length);
        out += this.renderer.html((function () {
            var words = cap[1].split('|');
            var base  = words.shift().split('');
            var furi  = words;

            if ( base.length !== furi.length ) {
                base = [ base.join('') ];
                furi = [ furi.join('') ];
            }

            var ret = '<ruby>';
            for ( var idx = 0, len = base.length; idx < len; idx++ ) {
                ret += self.output(base[idx]) + '<rt>' + self.output(furi[idx]) + '</rt>';
            }
            ret += '</ruby>';

            return ret;
        })());

        continue;
    }

    // text
*/}).toString().split('\n').slice(1,-1).join('\n');
    var InlineLexer = require('kramed/lib/lex/inline');
    var output      = new Function("src", InlineLexer.prototype.output.toString()
                                            .replace('// text', INSERT_CODE)
                                            .split('\n').slice(1,-1).join('\n'));

    InlineLexer.prototype.output = function (src) {
        this.rules = merge({}, this.rules, {
            furigana: FURIGANA,
            text: replace(this.rules.text)(']|', '{]|')()
        })

        return output.call(this, src);
    };

    __is_applied = true;
})();
