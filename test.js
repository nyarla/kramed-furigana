"use strict";

require('./index.js');

var kramed = require('kramed');
var assert = require('assert');

assert.equal(
    "<p><ruby>約束されし勝利の剣<rt>エクスカリバー</rt></ruby></p>\n",
    kramed('{約束されし勝利の剣|エクスカリバー}')
);

assert.equal(
    "<p><ruby>電<rt>でん</rt>子<rt>し</rt>書<rt>しょ</rt>籍<rt>せき</rt></ruby></p>\n",
    kramed('{電子書籍|でん|し|しょ|せき}')
);


